from applications.utility.models import BaseModel
from django.utils.translation import gettext_lazy as _
from django.db import models

class Batch(BaseModel):
    name = models.CharField(max_length=255,verbose_name=_('name'),null=False)