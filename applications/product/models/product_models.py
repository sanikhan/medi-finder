from django.db import models

from applications.utility.models import BaseModel
from django.utils.translation import gettext_lazy as _


class Product(BaseModel):
    name = models.CharField(max_length=255,verbose_name= _('name'),null=False)
    product_batch = models.ForeignKey('product.Batch', null=True,blank=True,on_delete=models.CASCADE)
    description = models.TextField(max_length=500,verbose_name= _('Description'))