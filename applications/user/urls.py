from django.urls import path, include
from .routers import user_routers

app_name = 'user'
urlpatterns = [
    path('api/', include(user_routers.urls)),
]