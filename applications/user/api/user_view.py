from django.shortcuts import get_object_or_404
from rest_framework import viewsets
from rest_framework.authtoken.models import Token
from rest_framework.decorators import action
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from applications.user.api.user_serializer import UserSerializer
from applications.user.models import User
from applications.utility.otp_methods import otp_generate, otp_verification


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def get_permissions(self):
        if self.request.method == 'POST' and self.create:
            return [AllowAny(), ]
        return super(UserViewSet, self).get_permissions()

    @action(methods=['POST'], detail=False, permission_classes=[AllowAny, ])
    def login(self, request):
        user = get_object_or_404(User, mobile_number=request.data['mobile_number']
        if request.data['mobile_number'] else None)
        if user.check_password(request.data['password']
                               if request.data['password'] else None):
            token, _ = Token.objects.get_or_create(user=user)
            return Response({'Status': 'Successful', 'Token': token.key})
        return Response({'Status': 'Unsuccessful', 'Details': 'Password unmatched.'})

    @action(methods=['POST'], detail=False)
    def change_password(self, request):
        old_password = request.POST.get("old_password")
        new_password = request.POST.get("new_password")
        user_instance = get_object_or_404(User, mobile_number=request.user.mobile_number)
        valid = user_instance.check_password(old_password)
        if not valid:
            return Response({"status": "Invalid Password"})
        user_instance.set_password(new_password)
        user_instance.save()
        return Response({"status": "Password Changed Successfully"})

    @action(methods=['GET','POST'], permission_classes=[AllowAny,], detail=False,)
    def forget_password(self, request):
        if request.method == 'GET':
            user = get_object_or_404(User, mobile_number=request.GET.get('mobile_number'))
            otp_code = otp_generate(mobile_number=user.mobile_number)
            return Response({'Status': 'Successful', 'Details': 'OTP code has been sent to the mobile number.',
                             'Otp_code': otp_code})
        else:
            if otp_verification(mobile_number=request.data['mobile_number'],
                                code=request.data['code']):
                return Response({'Status': 'Successful', 'Details': 'Otp matched.'})
            return Response({'Status': 'Unsuccessful', 'Details': 'Otp not matched.'})

    @action(methods=['GET'], detail=False)
    def logout(self,request):
        token = Token.objects.get(user= request.user)
        token.delete()
        return Response({'Status': 'Successful', 'Details': 'successfully logged out.'})