from rest_framework import serializers

from applications.user.models import User


class UserSerializer(serializers.ModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name='user:user-detail')
    password = serializers.CharField(write_only=True,required=True)

    def create(self,validated_data):
        password = validated_data.pop('password')
        user = User.objects.create(**validated_data)
        if user:
            user.set_password(password)
            user.save()
            return user
        self.error_messages = 'Unable to create user'

    class Meta:
        model = User
        fields = ('pk', 'url', 'name', 'mobile_number', 'user_image', 'user_nid', 'is_verified', 'has_due', 'user_type', 'user_nid','password'
        )
