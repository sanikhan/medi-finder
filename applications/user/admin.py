from django.contrib import admin

# Register your models here.
from django.contrib.auth.models import Group

from applications.user.models import User

admin.site.register(User)
admin.site.unregister(Group)
admin.site.site_header = 'Medi Helper'
admin.site.site_title = ''