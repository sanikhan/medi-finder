from rest_framework import routers

from applications.user.api.user_view import UserViewSet

user_routers = routers.DefaultRouter()

user_routers.register(r'user',UserViewSet)