from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.models import AbstractUser
from django.db import models


class UserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, mobile_number, password, **extra_fields):
            """
            Create and save a user with the given username, email, and password.
            """
            if not mobile_number:
                raise ValueError('The given username must be set')
            # email = self.normalize_email(email)
            # username = self.model.normalize_username(mobile_number)
            user = self.model(mobile_number=mobile_number, **extra_fields)
            user.set_password(password)
            user.save(using=self._db)
            return user

    def create_user(self, mobile_number,password, **extra_fields):
            extra_fields.setdefault('is_staff', False)
            extra_fields.setdefault('is_superuser', False)
            extra_fields.setdefault('is_active', True)
            return self._create_user(mobile_number, password, **extra_fields)

    def create_superuser(self, mobile_number, password, **extra_fields):
            extra_fields.setdefault('is_staff', True)
            extra_fields.setdefault('is_superuser', True)
            extra_fields.setdefault('user_type',5)
            extra_fields.setdefault('is_active', True)

            if extra_fields.get('is_staff') is not True:
                raise ValueError('Superuser must have is_staff=True.')
            if extra_fields.get('is_superuser') is not True:
                raise ValueError('Superuser must have is_superuser=True.')

            return self._create_user(mobile_number, password, **extra_fields)


class User(AbstractUser):
    username = None
    name = models.CharField(max_length=50,null=True,blank=True)
    mobile_number = models.CharField(max_length=20,unique=True)
    USERNAME_FIELD = 'mobile_number'
    USER_TYPE_CHOICES = [
        (1, 'Shop User'),
        (2, 'Shop Customer'),
        (3, 'Staff'),
        (4, 'Admin'),
    ]
    user_type = models.PositiveSmallIntegerField(choices=USER_TYPE_CHOICES)
    user_image = models.ImageField(upload_to='user/image', null=True, blank=True)
    user_nid = models.CharField(max_length=30,null=True,blank=True)
    user_nid_image = models.ImageField(upload_to='user/image/nid',null=True,blank=True)
    is_verified = models.BooleanField(default=False)
    has_due = models.BooleanField(default=False,help_text='if user did not make payment this field will be True and he will be unable to log in')
    is_shop_user = models.BooleanField(default=False,null=True,blank=True)
    is_shop_customer = models.BooleanField(default=False,null=True,blank=True)
    is_staff = models.BooleanField(default=False,blank=True,null=True)
    is_admin = models.BooleanField(default=False,blank=True,null=True)
    # user_location =

    objects = UserManager()

    class Meta:
        verbose_name = "User"
        verbose_name_plural = 'Users'

    def __str__(self):
        return self.mobile_number

    def save(self, *args, **kwargs):
        if not self.id:
            if self.user_type == 1:
                self.is_shop_user = True
                self.is_active = True
            elif self.user_type ==2:
                self.is_shop_customer = True
                self.is_active = True
            elif self.user_type == 3:
                self.is__staff = True
                self.is_active = True
            elif self.user_type == 6:
                self.is_admin = True
                self.is_active = True

            return super(User, self).save(*args, **kwargs)
        return super(User, self).save(*args, **kwargs)
