# Generated by Django 3.0.5 on 2020-05-01 15:39

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Otp',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('mobile_number', models.CharField(max_length=11, unique=True)),
                ('otp_code', models.CharField(blank=True, max_length=10, null=True)),
                ('count', models.IntegerField(default=0, help_text='Number of otp sent')),
            ],
        ),
    ]
