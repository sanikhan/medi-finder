import random

from django.shortcuts import get_object_or_404
from rest_framework.response import Response

from applications.user.models import User
from applications.utility.models import Otp

MAX_OTP_COUNTER = 5  # maximum how many time otp_code will generate in a row


def otp_key(number):
    if number:
        key = random.randint(999, 9999)
        return key
    else:
        return False


def otp_generate(mobile_number):
    '''
    :param mobile_number: otp code will be generate for this mobile number and save in db
    :return: otp code
    '''
    if mobile_number:
        mobile_number = str(mobile_number)
        obj = Otp.objects.filter(mobile_number__exact=mobile_number).first()
        if obj:
            if obj.count > MAX_OTP_COUNTER:
                return Response({"Status": "Faild",
                                 "details": "OTP sent 5 times. please contact with support"})
            else:
                key = otp_key(mobile_number)
                obj.otp_code = key
                obj.count = obj.count + 1
                obj.save()
                return obj.otp_code
        else:
            key = otp_key(mobile_number)
            obj = Otp.objects.create(mobile_number=mobile_number, otp_code=key, count=1)
            return obj.otp_code


def otp_verification(mobile_number, code):
    '''
    :param mobile_number:
    :param code:
    :return: True is verified , False if not verified.
    '''
    obj = get_object_or_404(Otp, mobile_number=mobile_number)
    if obj:
        if obj.otp_code == code:
            user = User.objects.get(mobile_number=mobile_number)
            if not user.is_verified:
                user.is_verified = True
                user.save()
                obj.delete()
                return True
        else:
            return False
    else:
        return False
