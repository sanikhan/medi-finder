from django.db import models


class Otp(models.Model):
    mobile_number = models.CharField(max_length=11, unique=True)
    otp_code = models.CharField(max_length=10, blank=True, null=True)
    count = models.IntegerField(default=0, help_text='Number of otp sent')

    def __str__(self):
        return str(self.mobile_number) + 'is sent' + str(self.otp_code)